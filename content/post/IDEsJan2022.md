---
title: "Top IDEs and Editors for the year 2022."
author: "Theo"
type: ""
date: 2022-01-03T13:25:04+02:00
subtitle: "Developer environments for the new year."
image: ""
tags: ["ide", "editor", "code", "programming"]
---

Everyone has their favourite IDE or Editor, heres some of my favourites as of writing in January 2022.

<!--more-->

This list is my opinion and should in no way change yours, although I hope to help out if you're totally new to programming or just looking for another coding environment for the new year.

## Visual Studio Code 

[Vscode](https://code.visualstudio.com/) for short, is an open source and extensible text editor made by Microsoft with a ton of plugins and themes to truly make it suited to any project you want to take on and look good while doing it. With the amount of features you can add with plugins, you can be forgiven for mistaking this editor as an IDE. 

Vscode is free, multiplatform and used by millions of users monthly! For the above reasons, I feel safe in saying that it’s hands down my favourite editor to work on projects.

## Atom

[Atom](https://atom.io/) is, to quote the developers: “A hackable text editor for the 21st Century”.
This bold statement is in no way an empty boast as the developers from Github delivered a fully featured editor right out of the box. \
You would be right in assuming that atom is comes fully integrated with github’s version control considering who made the editor. 
Atom also boasts great packages and themes for users to install and explore.

Atom is free on all platforms.

## Sublime Text 4

[Sublime](https://www.sublimetext.com/) is a great text editor and is the GOTO editor for many a developer. Sublime is a light weight and extensible way to edit code in your projects.

Sublime is free to use as valuation project on all platforms, and as far as I know indefinitely, but you can support the developers of this great editor by buying the license.

## Vim/Neovim

The only reason I’m putting vim/neovim down so low is because of aproacability for new developers. [Vim](https://www.vim.org/) (my favourite flavour is [Neovim](https://neovim.io/)) is a editor thats installed on millions of machines around the world.

It’s extremely lightweight and customizable with one of its main features being key mappings and work flow that makes you extremely productive once mastered.

Although I must point out that there’s many great tutorials floating around Youtube and the internet on how to set up vim to suit your needs the best.

## Visual Studio 2022 and JetBrains IDEs

While I mention these two last, it by no means they are inferior to the above-mentioned editors. In fact, it’s the exact opposite! If that’s the case, then why mention them last you ask?
Well let's break them down one at a time.

[Visual Studio 2022](https://visualstudio.microsoft.com/), VS for short, is Microsoft’s latest version of their powerful flagship IDE. This programming environment that is full-featured and meant to be the only tool you will need to work on .Net, C#, F# and C++ applications. Although it must be said that you can work with other tech stacks as well, like JavaScript and Python. The community edition is free.

The reason I added this lower is because while available for Windows and Mac, there is no Linux version. The closest experience you would get on Linux would be JetBrain’s Rider.

And speaking of Rider, it’s one of [JetBrain’s many powerful IDE’s](https://www.jetbrains.com/). This is a company that makes IDEs for the world’s most popular and used programming languages. If you have a project to complete, chances are JetBrain’s has the right IDE for you!

Only downside is JetBrain’s subscription model. In a world of ever-increasing service and products as a subscription, adding one or two more subscriptions to keep track of can be a burden. I would rather pay an upfront amount to support developers than adding yet another subscription.

With all that being said, I do believe Visual Studio 2022 and JetBrain’s myriad of IDEs are well worth checking out if a editor does not cut it for you.

I hope you enjoyed my list of Editors/IDEs, there are many more that I didn’t cover in this post, but these are the ones I have personal experience with and are comfortable with reccocmending.

I hope every one of you will have a amazing 2022, happy coding!
