---
title: "Thanksgiving"
author: "Theo"
type: ""
date: 2021-11-24T14:27:31+02:00
subtitle: "Images in Markdown"
image: ""
tags: [misc, holiday, fun]
draft: true
---

Take a look at some of these Thanksgiving themed photos

<!--more-->

![happythanksgiving](https://images4.alphacoders.com/119/thumb-1920-1190488.jpg)

---

How about this one?

![another](https://images3.alphacoders.com/119/thumb-1920-1190487.jpg)

---

No?

Okay how about this one?

![last](https://images4.alphacoders.com/119/thumb-1920-1190484.jpg)

---

### According to wikipedia:

Thanksgiving is a national holiday celebrated on various dates in the United States, Canada, Grenada, Saint Lucia, and Liberia. It began as a day of giving thanks and sacrifice for the blessing of the harvest and of the preceding year. Similarly named festival holidays occur in Germany and Japan. Thanksgiving is celebrated on the second Monday of October in Canada and on the fourth Thursday of November in the United States and around the same part of the year in other places. Although Thanksgiving has historical roots in religious and cultural traditions, it has long been celebrated as a secular holiday as well.
