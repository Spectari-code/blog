---
title: "What is Game State?"
author: "Theo"
type: ""
date: 2021-12-23T11:28:08+02:00
bigimg: [{ src: "/img/gaming.jpg" }]
subtitle: "The use of state in games."
image: ""
tags: ["fundamentals", "code", "gamedev"]
---

<!--more-->

In simple terms it can be used to describe your game objects, as in, what state they are in, in any point of time in your game. \
Have you ever wondered how developers handled levels, menus, pauses and so forth? Well that’s all state logic!

How to implement game state will mostly depend on the developer and which tools/frameworks they are using.

For this post I will use [LOVE2D’s](https://love2d.org/) API (Application Programming Interface) as an example as Lua is an incredibly easy to read and follow even for beginners and game engines like Love do a lot of the heavy lifting for us.

Three important pieces of logic you will encounter in virtually any game made with Love is the logic that sets up your game, ```love.load``` and ```love.draw```.

What is a callback, you may ask? Well in very simple terms, it is functions that your code calls at certain times. \
These callbacks can either be called a single time, or multiple times.

In our case ```love.load``` will run once on startup to initialize our game,```love.update``` will then manage our game state frame by frame and finally ```love.draw``` is called afterwords to draw anything to the screen.

Some examples from the API documentation:

```lua
-- Load some default values for our rectangle.
function love.load()
    x, y, w, h = 20, 20, 60, 20
end

-- Increase the size of the rectangle every frame.
function love.update(dt)
    w = w + 1
    h = h + 1
end

-- Draw a coloured rectangle.
function love.draw()
    -- In versions prior to 11.0, color component values are (0, 102, 102)
    love.graphics.setColor(0, 0.4, 0.4)
    love.graphics.rectangle("fill", x, y, w, h)
end
```

We will explore game state with some more examples in future posts as we  delve deeper into LOVE2D game engine.
