---
title: About me
subtitle: Since you're here, I might aswell tell you a bit about myself
comments: false
---

My name is Theo, I also go my internet handle: Spectari/Spectari-code.

I'm a Developer and Blogger that loves to write feature-rich applications in a variety of different programming languages.
I also have a passion for game development.

Curious to see what I'm currently working on? [Click here](https://github.com/Spectari-code) or on the little Github icon at the bottom to visit my repositories.

For business inquiries, or maybe you just have questions about one of my posts, feel free to send me an email by clicking on the little mail icon below and I will get back to you as soon as possible.

If you feel like supporting this site, you can buy me a coffee using this [Ko-fi link.](https://ko-fi.com/facetech)
It's tottaly optional and up to you, I will always strive to bring you the best content possible regardless.
