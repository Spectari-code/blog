---
title: "Blog Posts on the way!"
author: "Theo"
type: ""
date: 2021-12-06T18:03:24+02:00
subtitle: "A quick announcement..."
image: ""
tags: ["maintenance", "housekeeping"]
---

This is Face-Tech's first blog post!

<!--more-->

Tech related posts to follow soon...
Watch this space! \
This blog is in the final stage of development.

~~Comments are disabled for now.~~ \
After much consideration and for various reasons, the comment system was removed.
